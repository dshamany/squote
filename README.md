# Getting Started

<img src="./squote-logo.svg">

## Download

Once installed, you can remove this folder.

### wget

```bash
wget https://gitlab.com/dshamany/squote
```

### git

```bash
git clone https://gitlab.com/dshamany/squote.git
```

## Purpose

This application was developed for practicing regex and libcurl using C++.

## Dependencies

- libcurl
- C++ compiler (C++ 11 and above)
- std::regex

## Installation

- Install the dependencies
- Run the Script

### Fedora

```bash
sudo dnf install libcurl-devel
```

### RHEL/CentOS

```bash
sudo yum install libcurl-devel
```

### Ubuntu + derivatives

```bash
sudo apt-get install libcurl-dev
```

### Installation Script

This installation will compile the program with libcurl. It will make an executable that it then moves to `/usr/bin/`. Add a `-v` to enable verbose mode if you wish.

```bash
cd /path/to/downloaded/folder
./install_squote.sh
```

### Uninstall

You only have to remove the executable from `/usr/bin/`.

```bash
sudo rm /usr/bin/squote
```

## Usage

```
--- input ---
squote TSLA


--- output ---
Tesla, Inc. (TSLA)
NasdaqGS - NasdaqGS Real Time Price. Currency in USD
--------------------------------------------
Price                   733.57
Prev. Open              732.39
Open                    732.25
Bid                     732.78 x 1200
Ask                     734.62 x 800
Day's Range             724.20 - 734.00
52-Week Range           329.88 - 900.40
Volume                  14,832,806
AVG Volume              20,236,260
Market Cap              734.868B
Beta (5Y Monthly)       1.96
P/E Ratio               386.70
EPS (TTM)               1.90
Earnings Dates          Oct 19, 2021
                        Oct 25, 2021
1 Yr Target Est.        701.85
--------------------------------------------
Source: https://finance.yahoo.com/quote/TSLA
```

## License

Copyright 2021 Daniel Shamany

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.