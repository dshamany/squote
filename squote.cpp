/*
    AUTHOR:         Daniel Shamany
    SQUOTE -        A CLI application to quickly see your stock quotes
                    It uses Yahoo Finance and only requires a symbol
    DEPENDENCIES:   C++ Standard Library and libcurl
    PURPOSE:        This application was developed for practicing regex 
                    and libcurl using C++.
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    LICENSE:        MIT License | Copyright 2021 Daniel Shamany

                    Permission is hereby granted, free of charge, to any 
                    person obtaining a copy of this software and associated 
                    documentation files (the "Software"), to deal in the
                    Software without restriction, including without 
                    limitation the rights to use, copy, modify, merge, 
                    publish, distribute, sublicense, and/or sell copies 
                    of the Software, and to permit persons to whom the 
                    Software is furnished to do so, subject to the 
                    following conditions:

                    The above copyright notice and this permission notice 
                    shall be included in all copies or substantial portions 
                    of the Software.

                    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY 
                    KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE 
                    WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
                    PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS 
                    OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
                    OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
                    OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
                    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*/

#include <curl/curl.h>
#include <iostream>
#include <regex>
#include <string>
#include <vector>
#include <sstream>

// function to write to string buffer
static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    ((std::string *)userp)->append((char *)contents, size * nmemb);
    return size * nmemb;
}

void appendMatchesToVector(std::string str, std::regex reg, std::vector<std::string> &vec)
{
    std::smatch matches;
    int n = 0;
    while (std::regex_search(str, matches, reg))
    {
        // 1 represents group number
        vec.push_back(matches.str(1));
        // remove matched prefix from string to proceed 
        // to the next one (avoiding this creates an 
        // infinite loop)
        str = matches.prefix().str();
    }
}

void padPrintLn(std::ostream& out, const std::string& line, const std::string& front = "", const std::string& back = ""){
    out << front << line << back;
}

std::string repeatedChar(const char& c, size_t n, bool add_newline = false){
    std::string str = "";
    while (n--){
        str += c;
    }
    
    if (add_newline){
        str += '\n';
    }

    return str;
}

std::string repeatedChar(const std::string& s, size_t n, bool add_newline = false){
    std::string str = "";
    while (n--){
        str += s;
    }
    
    if (add_newline){
        str += '\n';
    }

    return str;
}

int main(int argc, char *argv[])
{

    // formatting strings
    const std::string h_line = repeatedChar('-', 64, true);
    const std::string tabs   = repeatedChar('\t', 4);

    // ensure user has entered a symbol
    if (argc < 2){
        padPrintLn(std::cerr, "Symbol Required (e.g. \'IBM\').\n");
        return -1;
    }

    // init variables for curl
    CURL *curl = curl_easy_init();
    std::string readBuffer;
    CURLcode result;

    // handle curl exception
    if (!curl)
    {
        std::cerr << "Curl Init Failed.\n";
    }

    // create complete urlfor yahoo with ability to append quote
    std::string url = "https://finance.yahoo.com/quote/";
    url += argv[1];

    // set curl options
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    // write output to file as opposed to the default stdout
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);

    // perform request
    result = curl_easy_perform(curl);

    // handle failed result
    if (result != CURLE_OK)
    {
        std::string curl_error = curl_easy_strerror(result);
        padPrintLn(std::cerr, "Download Failed: " + curl_error + "\n");
    }

    // create a regex pattern to search each item in 
    // the html returned from yahoo finance 
    std::vector<std::string> patterns = {
        // symbol name
        "<h1 .*?>(.*?)</h1>",
        // currency
        "<span data-reactid=\"9\".*?>(.*?)</span>",
        // ticker price
        "class=\"Trsdu\\(0\\.3s\\) Fw\\(b\\) Fz\\(36px\\) Mb\\(-4px\\) D\\(ib\\)\".*?>.*?(.*?\\d\\.\\d+).*?<",
        // prev. open
        "<tbody.*?</td.*?data-reactid=\"97\".*?>(.*?)<",
        // open
        "<tbody.*?</td.*?data-reactid=\"102\".*?>(.*?)<",
        // bid
        "<tbody.*?</td.*?data-reactid=\"107\".*?>(.*?)<",
        //ask
        "<tbody.*?</td.*?data-reactid=\"112\".*?>(.*?)<",
        // day's range
        "<tbody.*?</td.*?data-reactid=\"116\".*?>(.*?)<",
        // 52-week range
        "<tbody.*?</td.*?data-reactid=\"120\".*?>(.*?)<",
        // volume
        "<tbody.*?</td.*?data-reactid=\"125\".*?>(.*?)<",
        // avg volume
        "<tbody.*?</td.*?data-reactid=\"130\".*?>(.*?)<",
        // market cap
        "<tbody.*?data-reactid=\"133\">.*?<td.*?data-reactid=\"138\".*?>(.*?)<",
        // beta (5Y monthly)
        "<tbody.*?data-reactid=\"133\">.*?<td.*?data-reactid=\"143\".*?>(.*?)<",
        // PE ratio
        "<tbody.*?data-reactid=\"133\">.*?<td.*?data-reactid=\"148\".*?>(.*?)<",
        // EPS (TTM)
        "<tbody.*?data-reactid=\"133\">.*?<td.*?data-reactid=\"153\".*?>(.*?)<",
        // earnings date
        "<tbody.*?data-reactid=\"133\">.*?<td.*?data-reactid=\"158\".*?>(.*?)<",
        "<tbody.*?data-reactid=\"133\">.*?<td.*?data-reactid=\"160\".*?>(.*?)<",
        // 1yr target estimate
        "<tbody.*?data-reactid=\"133\">.*?<td.*?data-reactid=\"173\".*?>(.*?)<",
    };

    // create labels for the table
    const std::vector<std::string> labels = {
        "\t\t",
        "\t\t",
        "Price\t\t",
        "Prev. Open\t",
        "Open\t\t",
        "Bid\t\t",
        "Ask\t\t",
        "Day\'s Range\t",
        "52-Week Range\t",
        "Volume\t\t",
        "AVG Volume\t",
        "Market Cap\t",
        "Beta (5Y Monthly)",
        "P/E Ratio\t",
        "EPS (TTM)\t",
        "Earnings Dates\t",
        "\t\t",
        "1 Yr Target Est."
    };

    // append matches to vector
    std::vector<std::string> matches;
    for (auto &p : patterns){
        // create pattern from array and store it in matches
        std::regex pattern(p, std::regex::icase);
        appendMatchesToVector(readBuffer, pattern, matches);
    }

    // clear console for readability
    system("reset");

    // print table using all matches from regex
    if (matches.size())
    {
        // initialize counter for labels
        int n = 0; 
        // loop over and attempt to center content
        // iterator
        std::vector<std::string>::iterator it;
        for (auto &s : matches)
        {
            if (n <= 1){
                std::cout << s << '\n';
                if (n == 1){
                    padPrintLn(std::cout, h_line);
                }
            } else {
                padPrintLn(std::cout, labels[n] + tabs + s + '\n');
            }
            n++;
        }
    } else {
        // handle error gracefully
        padPrintLn(std::cerr, "Symbol is invalid.\n");
    }

    // print footer
    padPrintLn(std::cout, h_line + "Source: " + url + "\n");
    // add new line and flush the buffer
    std::cout << std::endl;

    // remove handlers to prevent memory leaks
    curl_easy_cleanup(curl);
    
    return 0;
}
