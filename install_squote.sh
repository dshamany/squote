#! /usr/bin/bash

ARG=$1

# compile executable
g++ -std=c++11 $ARG -l curl squote.cpp -o squote

# make executable
sudo chmod +x squote

# move to user's bin
sudo mv squote /usr/bin/